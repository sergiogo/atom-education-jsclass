// En este caso ejemplo queremos obtener de un servicio simulado por la funcion "fetch" el numero de frutas de cada tipo,
// Y solo hasta que tenemos todos los numeros podemos realizar el proceso final que es una salida a consola con todos los datos.

// En este problema usamos promesas con la sintaxis (async/await)

function fetch(url) {
    return new Promise((resolve, reject) => {
        setTimeout(() => { resolve(Math.round(Math.random() * 10)) }, 1000)
    })
}
function store(name, amount) {
    console.log('Fruit stored', name, amount)
}

// Cada objeto almacenado es la promesa de cada petición asincrona. 
// En este ejemplo todos los procesos asyncronos son creados aparentemente en secuancia, sin embargo
// la palabra reservada await forza a que la siguiente linea no se ejecutara hasta que no se resuelva la promesa
// por lo que la cualidad de concurrencia se pierde.

async function start() {
    const apples = await fetch('/obtainApples')
    const pears = await fetch('/obtainPears')
    const melons = await fetch('/obtainMelons')
    const pineapples = await fetch('/obtainPineapples')
    const guabas = await fetch('/obtainGuabas')

    store('apples', apples)
    store('pineapples', pineapples)

    console.log('totalfruits=', apples + pears + melons + pineapples + guabas)
}

start()

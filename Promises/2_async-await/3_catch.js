// En este caso ejemplo queremos obtener de un servicio simulado por la funcion "fetch" el numero de frutas de cada tipo,
// Y solo hasta que tenemos todos los numeros podemos realizar el proceso final que es una salida a consola con todos los datos.

// En este problema usamos promesas con la sintaxis (async/await)

function fetch(url) {
    return new Promise((resolve, reject) => {
        setTimeout(() => { 
            const number = Math.round(Math.random() * 10)
            if (number > 1) {
                resolve(number) 
            } else {
                reject(number)
            }
        }, 1000)
    })
}

async function store(name, promise) {
    const amount = await promise
    if (amount > 0) {
        console.log('Fruit stored', name, amount)
    }
}


// Cada objeto almacenado es la promesa de cada petición asincrona. 
// En este ejemplo todos se requiere que todas las peticiones ocurran de manera secuencial y atómica (si una falla todo el proceso falla)
// Dado que es una funcion async todas las promesas "esperadas" con await, en caso de ser rechazadas lanzaran una excepción
// que debe ser atrapada con la sentencia try/catch

async function start() {
    try {
        const apples = await fetch('/obtainApples')
        store('apples', apples)

        const pears = await fetch('/obtainPears')
        const melons = await fetch('/obtainMelons')

        const pineapples = await fetch('/obtainPineapples')
        store('pineapples', pineapples)

        const guabas = await fetch('/obtainGuabas')

        console.log('totalfruits=', apples + pears + melons + pineapples + guabas)
    } catch(number) {
        console.log('Process failed')
    }
}

start()

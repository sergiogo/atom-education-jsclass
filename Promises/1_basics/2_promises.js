// En este caso ejemplo queremos obtener de un servicio simulado por la funcion "fetch" el numero de frutas de cada tipo,
// Y solo hasta que tenemos todos los numeros podemos realizar el proceso final que es una salida a consola con todos los datos.

// En este problema usamos promesas en vez de callbacks

function fetch(url) {
    return new Promise((resolve, reject) => {
        setTimeout(() => { resolve(Math.round(Math.random() * 10)) }, 1000)
    })
}
function store(name, amount) {
    console.log('Fruit stored', name, amount)
}

// Cada objeto almacenado es la promesa de cada petición asincrona. Esta promesa puede ser transferida y referenciada
// a discreción en el código y realmente solo crear las condiciones asociadas a su resolución cuando sea necesario.

// En este ejemplo todos los procesos asyncronos son creados e iniciados de manera simultanea.

function start() {
    const apples = fetch('/obtainApples')
    const pears = fetch('/obtainPears')
    const melons = fetch('/obtainMelons')
    const pineapples = fetch('/obtainPineapples')
    const guabas = fetch('/obtainGuabas')

    apples.then(amount => store('apples', amount))
    pineapples.then(amount => store('pineapples', amount))

    Promise.all([ apples, pears, melons, pineapples, guabas ]).then(function(results) {
        console.log('totalfruits=', results.reduce((ac, v) => ac + v, 0))
    });
}

start()

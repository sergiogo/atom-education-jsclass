// El problema llamado "Callback hell" o infierno de callbacks surge a medida que necesitamos implementar procesos asyncronos
// de los cuales depende nuestra ejecución de codigo por lo que debemos ejecutar las condiciones subsecuentes cuando el "callback"
// es invocado. A medida que estos proceso crecen tambien como quedana anidados los procesos dentro de funciones.

// En este caso ejemplo queremos obtener de un servicio simulado por la funcion "fetch" el numero de frutas de cada tipo,
// Y solo hasta que tenemos todos los numeros podemos realizar el proceso final que es una salida a consola con todos los datos.

// En este problema no usamos promesas, solo funciones callback

function fetch(url, onComplete, onError) {
    setTimeout(() => { onComplete(Math.round(Math.random() * 10)) }, 1000)
}

function store(name, amount) {
    console.log('Fruit stored', name, amount)
}

// En este ejemplo todos los proceso son creados en secuencia uno despues de otro para ir obteniendo cada
// resultado.
function start () {
    fetch('/obtainApples', function(apples){

        store('apples', apples)
    
        fetch('/obtainPears', function(pears) {
            fetch('/obtainMelons', function(melons){
                fetch('/obtainPineapples', function(pineapples){
    
                    store('pineapples', pineapples)
    
                    fetch('/obtainGuabas', function(guabas) {
                        
                        console.log('totalfruits=', apples + pears + melons + pineapples + guabas)
    
                    })
                })
            })
        })
    })
}

start()

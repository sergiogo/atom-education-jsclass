// En este caso ejemplo queremos obtener de un servicio simulado por la funcion "fetch" el numero de frutas de cada tipo,
// Y solo hasta que tenemos todos los numeros podemos realizar el proceso final que es una salida a consola con todos los datos.

// En este caso ejemplo queremos obtener de un servicio simulado por la funcion "fetch" el numero de frutas de cada tipo,
// Y solo hasta que tenemos todos los numeros podemos realizar el proceso final que es una salida a consola con todos los datos.
// Sin embargo nuestro servicio va a fallar si el numero de frutas es menor a 5, para esos casos queremos no contemplarlos para la suma final y además
// avisar a través de una salida a la consola que no hubo suficientes frutas de ese tipo.

// En este problema usamos promesas en vez de callbacks

function fetch(url) {
    return new Promise((resolve, reject) => {
        setTimeout(() => { 
            const number = Math.round(Math.random() * 10)
            if (number > 5) {
                resolve(number) 
            } else {
                reject(number)
            }
        }, 1000)
    })
}
function store(name, amount) {
    if (amount) {
        console.log('Fruit stored', name, amount)
    }
}

function handleError(fruit) {
    return function(amount) {
        console.log('Not enough', fruit, amount)
        return 0
    }
}

// Cada objeto almacenado es la promesa de cada petición asincrona. Esta promesa puede ser transferida y referenciada
// a discreción en el código y realmente solo crear las condiciones asociadas a su resolución cuando sea necesario.

// En este ejemplo todos los procesos asyncronos son creados e iniciados de manera simultanea.


function start() {
    const apples = fetch('/obtainApples').catch(handleError('apples'))
    const pears = fetch('/obtainPears').catch(handleError('pears'))
    const melons = fetch('/obtainMelons').catch(handleError('melons'))
    const pineapples = fetch('/obtainPineapples').catch(handleError('pineapples'))
    const guabas = fetch('/obtainGuabas').catch(handleError('guabas'))

    apples.then(amount => store('apples', amount))
    pineapples.then(amount => store('pineapples', amount))

    Promise.all([ apples, pears, melons, pineapples, guabas ]).then(function(results) {
        console.log('totalfruits=', results.reduce((ac, v) => ac + v, 0))
    })
}

start()

// El problema llamado "Callback hell" o infierno de callbacks surge a medida que necesitamos implementar procesos asyncronos
// de los cuales depende nuestra ejecución de codigo por lo que debemos ejecutar las condiciones subsecuentes cuando el "callback"
// es invocado. A medida que estos proceso crecen tambien como quedana anidados los procesos dentro de funciones.

// En este caso ejemplo queremos obtener de un servicio simulado por la funcion "fetch" el numero de frutas de cada tipo,
// Y solo hasta que tenemos todos los numeros podemos realizar el proceso final que es una salida a consola con todos los datos.
// Sin embargo nuestro servicio va a fallar si el numero de frutas es menor a 5, para esos casos queremos no contemplarlos para la suma final y además
// avisar a través de una salida a la consola que no hubo suficientes frutas de ese tipo.

// En este problema no usamos promesas, solo funciones callback, además se implementan callbacks para los casos de error.

function fetch(url, onComplete, onError) {
    setTimeout(() => { 
        const number = Math.round(Math.random() * 10)
        if (number > 5) {
            onComplete(number) 
        } else {
            onError(number)
        }
    }, 1000)
}

function store(name, amount) {
    console.log('Fruit stored', name, amount)
}

// En este ejemplo todos los proceso son creados en secuencia uno despues de otro para ir obteniendo cada
// resultado.
function start () {
    const enoughapples = 0
    const enoughpears = 0
    const enoughmelons = 0
    const enoughpineapples = 0
    const enoughguabas = 0

    fetch('/obtainApples', function(apples){

        store('apples', apples)
        enoughapples = apples
    
        fetch('/obtainPears', function(pears) {
            enoughpears = pears

            fetch('/obtainMelons', function(melons){
                enoughmelons = melons
                fetch('/obtainPineapples', function(pineapples){
                    enoughpineapples = pineapples
                    store('pineapples', pineapples)
    
                    fetch('/obtainGuabas', function(guabas) {
                        enoughguabas = guabas
                        console.log('totalfruits=', enoughapples + enoughpears + enoughmelons + enoughpineapples + enoughguabas)
    
                    })
                })
            })
        }, function(pears) {
            console.log('peras no suficientes', pears)
        })
    }, function(apples){
        console.log('manzanas no suficientes', apples)

        fetch('/obtainPears', function(pears) {
            enoughpears = pears

            fetch('/obtainMelons', function(melons){
                fetch('/obtainPineapples', function(pineapples){
    
                    store('pineapples', pineapples)
    
                    fetch('/obtainGuabas', function(guabas) {
                        
                        console.log('totalfruits=', enoughapples + enoughpears + enoughmelons + enoughpineapples + enoughguabas)
    
                    })
                })
            })
        }, function(pears) {
            console.log('peras no suficientes', pears)

            fetch('/obtainMelons', function(melons){
                fetch('/obtainPineapples', function(pineapples){
    
                    store('pineapples', pineapples)
    
                    fetch('/obtainGuabas', function(guabas) {
                        
                        console.log('totalfruits=', enoughapples + enoughpears + enoughmelons + enoughpineapples + enoughguabas)
    
                    })
                })
            })
        })
    })
}


start()

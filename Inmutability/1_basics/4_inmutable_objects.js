// En javascript los objetos mapa que comunmente se les llaman solo "Objeto" tienen el mismo comportamiento que los arreglos
// son objetos mutables. Para crear objetos inmutables podemos usar la funcion Freeze en Object.
// En este ejemplo tenemos dos objetos persona con la misma estructura pero con naturalezas de mutabilidad distintas.

// ImmutableMap

const persona = { nombre: 'Carlos', apellido: 'Calabza', edad: 18 }
const juan = Object.freeze({ nombre: 'Juan', apellido: 'Vazquez', edad: 33 })


console.log('persona=',persona)
console.log('juan=',juan)

// Persona va a mutar sus valores
persona.nombre = 'Juancho'
persona.apellido = 'Mazorca'

// Juan no puede ser mutado
juan.nombre = 'Pedro'
juan.apellido = 'Pepino'

console.log('persona=', persona)
console.log('juan=', juan)
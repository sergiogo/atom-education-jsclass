// ---------- Array ----------------
// Los arreglos en javascript son objetos MUTABLES, por lo tanto cualquier asignación de valor a una nueva variable se está
// copiando la referencia del objeto y cualquier cambio sobre el objeto no creará un nuevo objeto.
console.log('------ Array ----->')

const nombres = ['Juan', 'Carlos', 'Pedro']
const referencia1 = nombres
const referencia2 = referencia1
const referencia3 = referencia2

console.log(nombres)
console.log(referencia1)
console.log(referencia2)
console.log(referencia3)

// Cambiemos el original

nombres[1] = 'Goku'

console.log(nombres)
console.log(referencia1)
console.log(referencia2)
console.log(referencia3)

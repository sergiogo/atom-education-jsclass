// Los tipos de datos primitivos son inmutables en javascript.

// --------- string -----------------
// Las funciones o procesos que ejecuten cambios sobre mi variable titulo no alteran el valor original. 
// La cadena creada originalmente no puede ser mutada.
console.log('------ string ------->')

const original = 'Mi obra literaria'
const alterado = original.replace(/i/g, '7')


console.log('alterado=', alterado)
console.log('original=', original)

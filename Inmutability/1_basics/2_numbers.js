// ---------- number ----------------
// Las variables asociadas a los objetos inmutables crean objetos nuevos cuando son asignados, eso incluye reasignacion por medio
// de una variable nueva. En este caso "suma" se define como un objeto nuevo cuyo binario es el mismo que "numero", cualquier intento
// de cambio despues de esto no cambia el valor original.
console.log('------ number ----->')

let numero = 8
let suma = numero

console.log('numero=', numero)
console.log('suma=', suma)

suma = numero + 10

console.log('numero=', numero)
console.log('suma=', suma)
// El como funciona la mutabilidad e inmutabilidad ofrecen la oportunidad de entender si un objeto cambió utilizando solamente operadores de igualdad
// sin necesidad de realizar una introspección

let persona1 = { nombre: 'Juan', apellido: 'Vazquez', edad: 33 }
let persona2 = persona1

let inmutable1 = Object.freeze({ nombre: 'Juan', apellido: 'Vazquez', edad: 33 })
let inmutable2 = inmutable1


console.log('Son iguales las personas?', persona1 === persona2)
console.log('Son iguales los inmutables?', inmutable1 === inmutable2)

// Cambios

persona2.nombre = 'Carlos'
inmutable2 = { ...inmutable2, nombre: 'Carlos' }

console.log('Son iguales las personas?', persona1 === persona2)
console.log('Son iguales los inmutables?', inmutable1 === inmutable2)